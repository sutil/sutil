package br.com.sutil.app.login.seguranca.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.sutil.app.login.seguranca.Usuario;
import br.com.sutil.repository.ListQueryDslPredicateExecutor;

public interface UsuarioRepository extends JpaRepository<Usuario , Long>, ListQueryDslPredicateExecutor<Usuario> {

}
