package br.com.sutil.app.imagem.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.sutil.app.imagem.Imagem;
import br.com.sutil.repository.ListQueryDslPredicateExecutor;

public interface ImagemRepository extends JpaRepository<Imagem, Long>, ListQueryDslPredicateExecutor<Imagem>{

}
