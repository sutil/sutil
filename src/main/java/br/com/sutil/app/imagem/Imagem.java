package br.com.sutil.app.imagem;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.google.common.base.Objects;

@Entity
public class Imagem implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	private Long id;
	
	private String path;
	
	private String fileName;
	
	private String page;
	
	Imagem() {
	}
	
	private Imagem(String path, String fileName, String page){ 
		this.path = path;
		this.page = page;
		this.fileName = fileName;
	}
	
	public static Imagem newInstance(String path, String fileName, String page){
		checkNotNull(path, fileName, page);
		return new Imagem(path, fileName, page);
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("path", path).add("page", page).add("filename", fileName).toString();
	}
	
	@Override
	public int hashCode() {
		return Objects.hashCode(path, fileName);
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Imagem){
			Imagem other = (Imagem) obj;
			return Objects.equal(this.path, other.page) &&
				   Objects.equal(this.fileName, other.fileName);
		}
		return false;
	}
	
	public Long getId() {
		return id;
	}
	
	public String getPage() {
		return page;
	}
	
	public String getPath() {
		return path;
	}
	
	public String getFileName() {
		return fileName;
	}

}
