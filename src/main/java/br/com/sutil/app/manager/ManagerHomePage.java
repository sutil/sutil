package br.com.sutil.app.manager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;

import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.imageio.stream.FileImageInputStream;
import javax.servlet.ServletContext;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

import br.com.sutil.app.imagem.Imagem;
import br.com.sutil.app.imagem.repository.ImagemRepository;

@Service
@SessionScoped
public class ManagerHomePage implements Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	private ImagemRepository imagemRepository;

	public List<Imagem> getImagens() {
		return imagemRepository.findAll();
	}
	
//	public List<StreamedContent> getImgs() throws FileNotFoundException{
//		List<StreamedContent> images = Lists.newLinkedList();
//		List<Imagem> lista = imagemRepository.findAll();
//		for(Imagem img: lista){
//			System.out.println(img.getPath());
//			FileInputStream fileInputStream = new FileInputStream(img.getPath()+"/"+img.getFileName());
//			
//			StreamedContent image = new DefaultStreamedContent(fileInputStream, "image/jpeg");
//			images.add(image);
//		}
//		return images;
//	}

	@Transactional
	public void imageUploadListener(FileUploadEvent event) {
		try {
//			String path = System.getProperty("user.dir") + "/mvd/home/"+ event.getFile().getFileName();
			FacesContext ctx = FacesContext.getCurrentInstance();
			ServletContext context = (ServletContext) ctx.getExternalContext().getContext();
			String path = context.getRealPath("/home/");
			System.out.println("path___>"+path);
			File dir = new File(path);
			if(!dir.exists()){
				dir.mkdirs();
			}
			FileOutputStream fileOutputStream = new FileOutputStream(path+"/"+event.getFile().getFileName());
			byte[] buffer = new byte[1024];
			int bulk;
			InputStream inputStream = event.getFile().getInputstream();
			while (true) {
				bulk = inputStream.read(buffer);
				if (bulk < 0) {
					break;
				}
				fileOutputStream.write(buffer, 0, bulk);
				fileOutputStream.flush();
			}

			fileOutputStream.close();
			inputStream.close();
			Imagem img = Imagem.newInstance(path, event.getFile().getFileName(), "home");
			imagemRepository.save(img);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
