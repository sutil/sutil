package br.com.sutil.app.site;

import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;
import org.springframework.stereotype.Component;

@Component
public class MapController {
	
	public MapModel getPosition(){
		MapModel model = new DefaultMapModel();
		
		LatLng coordenada = new LatLng(-23.421156,-51.948169);
		
		model.addOverlay(new Marker(coordenada, "MV Decorações"));
		
		return model;
		
	}
}
