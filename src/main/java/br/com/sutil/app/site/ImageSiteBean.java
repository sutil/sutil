package br.com.sutil.app.site;

import java.util.List;

import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;

@Component
public class ImageSiteBean {
	
	public List<String> images(){
		List<String> images = Lists.newLinkedList();
		images.add("img1.jpg");
		images.add("img2.jpg");
		images.add("img3.jpg");
		images.add("img4.jpg");
		return images;
	}
}
