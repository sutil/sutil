package br.com.sutil.app.site.book;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

@ManagedBean
public class BookManagerBean implements Serializable{
	
	public BookManagerBean() {
	}
	
	private UploadedFile file;

	private static final long serialVersionUID = 1L;
	
	public void handleFileUpload(FileUploadEvent event) { 
		System.out.println("teste ok");
    }
	
	public UploadedFile getFile() {
		return file;
	}
	public void setFile(UploadedFile file) {
		this.file = file;
	}
	
	public void teste(ActionEvent event){
		System.out.println("???");
	}
	
	public void upload(ActionEvent event){
		System.out.println("?");
		if(file != null) {  
            FacesMessage msg = new FacesMessage("Succesful", file.getFileName() + " is uploaded.");  
            FacesContext.getCurrentInstance().addMessage(null, msg);  
        }  
	}
	
	public String getTeste(){
		return "teste";
	}

}
