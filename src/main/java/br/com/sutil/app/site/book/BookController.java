package br.com.sutil.app.site.book;

import org.primefaces.event.FileUploadEvent;
import org.springframework.stereotype.Controller;

@Controller
public class BookController {
	
	public BookManagerBean newBookManager(){
		System.out.println("retornando manager book");
		return new BookManagerBean();
	}
	
	public void handleFileUpload(FileUploadEvent event) { 
		System.out.println("teste ok");
    }

}
